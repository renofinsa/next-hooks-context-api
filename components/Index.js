import Link from 'next/link'
import {useContext, useEffect} from 'react'
import IndexState, {IndexContext} from './../context/IndexState'

const Index = () => {
	const {
		data,
		state,
		get_data_users
	} = useContext(IndexContext)
	
	useEffect(() => {
		get_data_users()
	}, [])
	
	// const Loading = state.loading ? <h1>Loading ...</h1> : ''
		// console.log(state.loading);
  return (
    <IndexState>
			{/* <Loading /> */}
      {state.data.map((item, index) => (
				<Link key={index} href="/user/[id]" as={`/user/${item.id}`}>
					<div>
					<a>{item.name}</a>
					<hr />
					</div>
				</Link>
			))}
    </IndexState>
  )
}

// Index.getInitialProps = async (ctx) => {
// 	const {
// 		data,
// 		state,
// 		get_data_users
// 	} = useContext(IndexContext)

// 	useEffect(() => {
// 		get_data_users()
// 		console.log(data);
// 	}, [])

// 	return {fetch: state}
// }

export default Index