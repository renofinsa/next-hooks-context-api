import {
	FETCH_DATA_USER,
	FETCH_DETAIL_USER
} from './IndexTypes'

export default (state, {type, payload}) => {
	switch (type) {
		case FETCH_DATA_USER:
			return {
				...state,
				isAuthenticated: false,
				data: payload,
				item: ''
			}
		case FETCH_DETAIL_USER:
			return {
				...state,
				isAuthenticated: false,
				item: payload
			}
		default:
			break;
	}
}