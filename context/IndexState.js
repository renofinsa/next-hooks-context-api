import React, { createContext, useReducer, useState, useEffect } from 'react'

import IndexReducer from './IndexReducer'

export const IndexContext = createContext();

import {
	FETCH_DATA_USER,
	FETCH_DETAIL_USER
} from './IndexTypes'

const IndexState = ({ children }) => {
	const initialStateIndexReducer = {
		isAuthenticated: false,
		data: [],
		item: [],
	}

	const [loading, setLoading] = useState(1)
	const [state, dispatch] = useReducer(IndexReducer, initialStateIndexReducer)

	const get_data_users = async () => {
		await setLoading(1)

		try {
			fetch('https://jsonplaceholder.typicode.com/users')
			.then(response => response.json())
			.then(json => {
			 dispatch({ type: FETCH_DATA_USER, payload: json})
			 setLoading(0)
			})

		} catch (error) {
			console.log(error.message);			
		}
	}

	const get_detail_users = async (id) => {
		await setLoading(1)

		try {
			fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
			.then(response => response.json())
			.then(json => {
			 dispatch({ type: FETCH_DETAIL_USER, payload: json})
			 setLoading(0)
			})

		} catch (error) {
			console.log(error.message);			
		}
	}

	const {
	isAuthenticated,
	data,
	item,
	} = state

	return (
		<IndexContext.Provider
			value={{
				data,
				loading,
				state,
				item,
				get_data_users,
				get_detail_users
			}}
		>
			{children}
		</IndexContext.Provider>
	)
}

export default IndexState
