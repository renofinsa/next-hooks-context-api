import '../styles/globals.css'
import IndexState from '../context/IndexState'

function MyApp({ Component, pageProps }) {
  return (
		<IndexState>
			<Component {...pageProps} />
    </IndexState>
	) 
	
}

export default MyApp
