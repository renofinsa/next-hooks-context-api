import Head from 'next/head'
import styles from '../styles/Home.module.scss'
import Users from './Users'


export default function Home() {
  return (
    <div className={styles.container}>
		<Users />
    </div>
  )
}
