import { withRouter } from 'next/router'
import {useContext, useEffect} from 'react'

import {IndexContext} from './../context/IndexState'
	
function User({ router, id }) {
	const {
		item,
		state,
		loading,
		get_detail_users
	} = useContext(IndexContext)

	useEffect(() => {
		get_detail_users(id)
	}, [])

	return (
		<div>
		{loading ? <h1>Loading ...</h1>
			:
				<Main item={item} />
			}
		</div>
	)
}

User.getInitialProps = async ({ query }) => {
	return { id: query.id }
}

const Main = ({item}) => (
	<div>
		<p>Name: {item.name}</p>
		<p>Username: {item.username}</p>
		<p>Email: {item.email}</p>
	</div>
)

export default withRouter(User)
