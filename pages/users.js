import Link from 'next/link'
import {useContext, useEffect} from 'react'
import IndexState, {IndexContext} from './../context/IndexState'

const Users = ({ }) => {

	const {
		data,
		state,
		loading,
		get_data_users
	} = useContext(IndexContext)

	const list = data || []

	useEffect(() => {
		get_data_users()
	}, [])
	
	if (loading) {
		return <h1>Loading ...</h1>
	}

  return (
    <IndexState>
      {loading ? <h1>Loading ...</h1>
			:
				data.map((item, index) => (
					<Link key={index} href={`/user/?id=${item.id}`} as={`/user/${item.id}`}>
						<div>
						<a>{item.name}</a>
						<hr />
						</div>
					</Link>
				))
			}
    </IndexState>
  )
}

Users.getInitialProps = async () => {
	
}

export default Users